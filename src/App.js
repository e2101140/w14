import React, { useState } from "react";
import "./App.css";

function App() {
  const [inputValue, setInputValue] = useState("");

  // Writes data to localStorage
  function sendChat() {
    const chats = JSON.parse(localStorage.getItem("chats")) || [];
    chats.push(inputValue);
    localStorage.setItem("chats", JSON.stringify(chats));
    setInputValue("");
  }

  // Reads data from localStorage and logs it to the console
  function showMessages() {
    const chats = JSON.parse(localStorage.getItem("chats")) || [];
    console.log(chats);
  }

  return (
    <div className="App">
      {/* Generating Lorem Ipsum content */}
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Elit ut aliquam
        purus sit amet. Et magnis dis parturient montes nascetur. Sit amet
        luctus venenatis lectus magna fringilla urna porttitor. Amet nulla
        facilisi morbi tempus iaculis urna id. Tincidunt dui ut ornare lectus
        sit amet est placerat. Quam pellentesque nec nam aliquam sem et tortor
        consequat. Eget dolor morbi non arcu. Nam aliquam sem et tortor
        consequat id porta nibh. Iaculis nunc sed augue lacus viverra vitae
        congue. Mauris rhoncus aenean vel elit scelerisque mauris. Tempor
        commodo ullamcorper a lacus. At tempor commodo ullamcorper a lacus
        vestibulum sed arcu. Ut enim blandit volutpat maecenas volutpat blandit
        aliquam etiam. Sem viverra aliquet eget sit amet tellus cras adipiscing
        enim. Adipiscing diam donec adipiscing tristique risus nec.
      </p>
      <p>
        Tempus egestas sed sed risus. Orci porta non pulvinar neque laoreet
        suspendisse interdum consectetur libero. Sit amet mattis vulputate enim
        nulla aliquet porttitor lacus luctus. Egestas dui id ornare arcu odio ut
        sem nulla pharetra. Suspendisse in est ante in nibh mauris cursus mattis
        molestie. Amet mattis vulputate enim nulla aliquet porttitor lacus.
        Tellus in hac habitasse platea dictumst vestibulum rhoncus est
        pellentesque. Elementum sagittis vitae et leo. Et netus et malesuada
        fames ac turpis egestas integer eget. Sapien eget mi proin sed libero
        enim sed faucibus. Malesuada fames ac turpis egestas maecenas pharetra
        convallis. Sodales ut etiam sit amet nisl purus in mollis nunc. Massa
        enim nec dui nunc mattis enim ut tellus. Adipiscing enim eu turpis
        egestas pretium. Interdum consectetur libero id faucibus nisl. Lectus
        arcu bibendum at varius vel. Non enim praesent elementum facilisis leo
        vel fringilla est ullamcorper. Bibendum enim facilisis gravida neque
        convallis a cras. Nec dui nunc mattis enim.
      </p>
      <p>
        Arcu ac tortor dignissim convallis. Est placerat in egestas erat.
        Consequat interdum varius sit amet mattis vulputate. Varius morbi enim
        nunc faucibus a pellentesque sit amet porttitor. Vitae et leo duis ut.
        Adipiscing bibendum est ultricies integer quis auctor. Aliquet bibendum
        enim facilisis gravida neque convallis a. Egestas integer eget aliquet
        nibh praesent tristique. Sit amet purus gravida quis blandit turpis. Nec
        feugiat in fermentum posuere urna nec tincidunt praesent. Phasellus
        faucibus scelerisque eleifend donec pretium vulputate sapien. Aliquam id
        diam maecenas ultricies. Posuere lorem ipsum dolor sit amet consectetur
        adipiscing. Nunc non blandit massa enim nec dui nunc. Tempor orci eu
        lobortis elementum nibh tellus molestie nunc.
      </p>
      <p>
        Eget dolor morbi non arcu risus quis varius. Cras ornare arcu dui
        vivamus arcu felis. Ut morbi tincidunt augue interdum velit euismod.
        Risus viverra adipiscing at in. Vitae justo eget magna fermentum iaculis
        eu non diam phasellus. Vitae turpis massa sed elementum tempus egestas
        sed sed risus. Egestas sed sed risus pretium quam vulputate dignissim
        suspendisse in. Nunc sed velit dignissim sodales ut eu sem integer
        vitae. Pharetra vel turpis nunc eget lorem. Diam phasellus vestibulum
        lorem sed risus. Volutpat est velit egestas dui id. Massa tempor nec
        feugiat nisl. Aliquet risus feugiat in ante metus dictum at tempor. Amet
        facilisis magna etiam tempor orci. Viverra ipsum nunc aliquet bibendum
        enim facilisis gravida neque convallis. Eget dolor morbi non arcu risus
        quis varius. Lorem ipsum dolor sit amet consectetur adipiscing elit.
        Arcu cursus vitae congue mauris rhoncus aenean vel. Sollicitudin nibh
        sit amet commodo nulla facilisi. Risus ultricies tristique nulla aliquet
        enim tortor at auctor urna.
      </p>
      <p>
        Quis enim lobortis scelerisque fermentum dui faucibus in. Nam libero
        justo laoreet sit amet cursus sit amet. Sapien eget mi proin sed libero
        enim sed. Ipsum nunc aliquet bibendum enim facilisis. Lectus magna
        fringilla urna porttitor rhoncus dolor purus. Auctor neque vitae tempus
        quam. Leo vel fringilla est ullamcorper eget nulla. At auctor urna nunc
        id cursus. Purus semper eget duis at tellus at urna. Vel pretium lectus
        quam id leo in vitae turpis massa. Erat nam at lectus urna. Auctor augue
        mauris augue neque gravida. Nisl rhoncus mattis rhoncus urna. Faucibus
        nisl tincidunt eget nullam non nisi est. Faucibus vitae aliquet nec
        ullamcorper sit. Aliquam malesuada bibendum arcu vitae elementum
        curabitur. Enim sit amet venenatis urna cursus eget. Rhoncus urna neque
        viverra justo nec.
      </p>

      {/* Chat input and buttons */}
      <div className="chat-box">
        <input
          type="text"
          id="chat"
          value={inputValue}
          onChange={(e) => setInputValue(e.target.value)}
        />
        <button onClick={sendChat}>Send</button>
        <button onClick={showMessages}>Show messages</button>
      </div>
    </div>
  );
}

export default App;
